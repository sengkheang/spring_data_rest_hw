package com.example.demo.repository;

import com.example.demo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface CustomerRepo extends JpaRepository<Customer, Integer> {

    Customer findCustomerByName(@Param("name") String name);

    List<Customer> findCustomerByAge(@Param("age") Integer age);
}
