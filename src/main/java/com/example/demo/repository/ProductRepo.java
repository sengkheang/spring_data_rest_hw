package com.example.demo.repository;

import com.example.demo.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface ProductRepo extends CrudRepository<Product, Integer> {

    Product findProductByName(@Param("name") String name);
    Product findProductByCode(@Param("code") String code);
    Product findProductByCategory(@Param("category") String category);
}
