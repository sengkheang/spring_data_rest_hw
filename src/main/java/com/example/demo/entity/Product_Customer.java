package com.example.demo.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Product_Customer implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "pro_Id")
    private Product product;

    @Id
    @ManyToOne
    @JoinColumn(name = "cust_Id")
    private Customer custId;

    private Integer qty;

    private Integer amount;

    @Column(columnDefinition = "Decimal(10,2)")
    private Double price;

    public Product_Customer() {
    }

    @Override
    public String toString() {
        return "Product_Customer{" +
                "product=" + product +
                ", custId=" + custId +
                ", qty=" + qty +
                ", amount=" + amount +
                ", price=" + price +
                '}';
    }

    public Product_Customer(Product product, Customer custId, Integer qty, Integer amount, Double price) {
        this.product = product;
        this.custId = custId;
        this.qty = qty;
        this.amount = amount;
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustId() {
        return custId;
    }

    public void setCustId(Customer custId) {
        this.custId = custId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
