package com.example.demo.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Product{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;
    private String code;

    @Column(name = "stock_price", columnDefinition = "Decimal(10,2)")
    private Double stockPrice;

    @Column(name = "stock_qty")
    private Integer stockQty;

    @Column(name = "import_date")
    private Date date;

    @OneToOne(cascade = CascadeType.ALL)
    private Category category;

    public Product(String name, String code, Double stockPrice, Integer stockQty, Date date, Category category) {
        this.name = name;
        this.code = code;
        this.stockPrice = stockPrice;
        this.stockQty = stockQty;
        this.date = date;
        this.category = category;
    }

    public Product() {
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", stockPrice=" + stockPrice +
                ", stockQty=" + stockQty +
                ", date=" + date +
                ", category=" + category +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(Double stockPrice) {
        this.stockPrice = stockPrice;
    }

    public Integer getStockQty() {
        return stockQty;
    }

    public void setStockQty(Integer stockQty) {
        this.stockQty = stockQty;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}