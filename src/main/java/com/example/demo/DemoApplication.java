package com.example.demo;

import com.example.demo.entity.Category;
import com.example.demo.entity.Customer;
import com.example.demo.entity.Product;
import com.example.demo.entity.Product_Customer;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Date;

@SpringBootApplication
@Transactional
public class DemoApplication implements ApplicationRunner {

    @PersistenceContext
    private EntityManager entityManager;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
//         LocalDate date=LocalDate.now();

        Customer customer1=new Customer();
        customer1.setName("Tata");
        customer1.setAge(23);
        customer1.setAddress("Phnom Penh");
        customer1.setContact("012 345 678");
        customer1.setGender("Female");

        Customer customer2=new Customer();
        customer2.setName("Lala");
        customer2.setAge(23);
        customer2.setAddress("Phnom Penh");
        customer2.setContact("012 345 678");
        customer2.setGender("Female");

        Customer customer3=new Customer();
        customer3.setName("Nana");
        customer3.setAge(23);
        customer3.setAddress("Phnom Penh");
        customer3.setContact("012 345 678");
        customer3.setGender("Female");

        Category category1=new Category();
        category1.setCateName("Electronic");

        Category category2=new Category();
        category2.setCateName("Phone");

        Product product1=new Product();
        product1.setCode("p01234");
        product1.setName("computer");
        product1.setDate(new Date());
        product1.setStockPrice(1500.10);
        product1.setStockQty(1);
        product1.setCategory(category1);

        Product product2=new Product("Hello", "p01235",  2000.3, 10, new Date(), category1);

        Product_Customer product_customer1=new Product_Customer();
        product_customer1.setCustId(customer1);
        product_customer1.setProduct(product1);
        product_customer1.setPrice(3000.20);
        product_customer1.setQty(2);
        product_customer1.setAmount(6000);

        Product_Customer product_customer2=new Product_Customer(product2, customer3, 3,18000 ,6000.9 );

        entityManager.persist(customer1);
        entityManager.persist(customer2);
        entityManager.persist(customer3);

        entityManager.persist(category1);
        entityManager.persist(category2);

        entityManager.persist(product1);
        entityManager.persist(product2);
        entityManager.persist(new Product("iphone", "p01236", 1200.0, 1, new Date(), category2));

        entityManager.persist(product_customer1);
        entityManager.persist(product_customer2);

    }
}
